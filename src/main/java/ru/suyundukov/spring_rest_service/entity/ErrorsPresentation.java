package ru.suyundukov.spring_rest_service.entity;

import java.util.List;

public record ErrorsPresentation(List<String> errors) {
}
