package ru.suyundukov.spring_rest_service.entity;

public record NewTaskPayload(String details) {
}
